#CodeTransfer

This is a plugin for the cloud IDE Codiad. It allows FTP or SCP connection to remote servers.

##Installation

- Download the zip file and unzip it to your plugin folder.
- Enable this plugin in the plugin manager.

##Features

- Filesystem naviagtion
- Up- and downloading files
- Create directories
- Change file permission
- Delete files and directories
- Rename files and directories
- Display file information
- Multiselection

##SCP/SFTP

- OpenSSL and libssh2 are required
- Installation: [http://de2.php.net/manual/en/ssh2.installation.php](Installation on php.net "More Information")

##Screenshots

#####FTP
![Screen](http://andrano.de/CodeTransfer/img/screen1.jpg "Screen")

#####SCP
![Screen](http://andrano.de/CodeTransfer/img/screen2.jpg "Screen")

###More Information

[http://andrano.de/CodeTransfer](http://andrano.de/CodeTransfer "More Information")


###Included

####CSS Loading animation
[http://codepen.io/rlemon/pen/KyDgh](CSS Loading "More Information")

#####Released under MIT License
[http://blog.codepen.io/legal/licensing/](License of CodePen "More Information")